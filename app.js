'use strict'

var express = require('express');
var bodyparser = require('body-parser');

var app= express();


//rutas

var rutasprod = require('./routes/products');
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.use(bodyparser.urlencoded({extended:false}))
app.use(bodyparser.json());

app.use('/', rutasprod);



module.exports = app;
