'use strict'


var express = require('express');
var ProductsController = require('../controllers/products');

var api = express.Router();

api.get('/pruebasgaby', ProductsController.pruebas);
api.get('/listadoProductos', ProductsController.allproducts);
api.post('/altaproductos', ProductsController.alta);
api.delete('/deleteProduct/:id',ProductsController.deleteProduct);

module.exports = api;