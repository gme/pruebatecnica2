'use stricts'


//modelos
var Producto = require('../models/product');


function pruebas(req,res){
    res.status(200).send({
        message:"probando controller products"
    })
}

function alta(req,res){
    var product = new Producto();

    var parametros = req.body;

    product.name        = parametros.name;
    product.description = parametros.description;
    product.quantity    = parametros.quantity;



    product.save((err, productStored)=>{
        if(err) {
            res.status(500).send({message:'ERROR al store product'})
        }
        else {
            if(!productStored){
                res.status(404).send({message:'No se ha registrado al producto'});
            }
            else{
                res.status(200).send({product:productStored});
            }
        }})


}

function allproducts(req, res){
    Producto.find({}, function(err, product) {
        if (err)
            res.send(err);
        res.json(product);
    });


}
function deleteProduct(req, res){

    var productId = req.params.id;
    Producto.findByIdAndRemove(productId,(err, productRemoved) =>{
        if(err){
            res.status(500).send({message:'error en la peticion de borrado'});
        }
        else{
            if(!productRemoved){
                res.status(404).send({message:' No se ha borrado al product'});
            }
            else{
                res.status(200).send({product:productRemoved})
            }

        }
    });

}


module.exports = {
    pruebas,
    alta,
    allproducts,
    deleteProduct
};